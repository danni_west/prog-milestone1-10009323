﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK25
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number then press <ENTER>.");

            var number = Console.ReadLine();
            var a = 0;
            bool value = int.TryParse(number, out a); 

            Console.WriteLine($"The number you typed in is '{number}'"); 
            Console.WriteLine($"But is this actually a number? {value}");
        }
    }
}
