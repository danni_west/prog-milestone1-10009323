﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK24__final_version_
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = 4;
            var mainmenu = "m";
            do
            {
                Console.Clear();

                Console.WriteLine($"          MAIN MENU           ");
                Console.WriteLine($"******************************");
                Console.WriteLine($"                              ");
                Console.WriteLine($"    1. Option 01    ");
                Console.WriteLine($"    2. Option 02    ");
                Console.WriteLine($"    3. Option 03    ");
                Console.WriteLine($"                              ");
                Console.WriteLine($"******************************");
                Console.WriteLine($"    Please select an option.  ");
                menu = int.Parse(Console.ReadLine());

                if (menu == 1)
                {
                    Console.Clear();

                    Console.WriteLine("You selected Option 01");
                    Console.WriteLine($"Please press \"m\" to return to the main menu");
                    mainmenu = (Console.ReadLine());
                }
                if (menu == 2)
                {
                    Console.Clear();

                    Console.WriteLine("You selected Option 02");
                    Console.WriteLine($"Please press \"m\" to return to the main menu");
                    mainmenu = (Console.ReadLine());
                }
                if (menu == 3)
                {
                    Console.Clear();

                    Console.WriteLine("You selected Option 03");
                    Console.WriteLine($"Please press \"m\" to return to the main menu");
                    mainmenu = (Console.ReadLine());
                }

            }
            while (mainmenu == "m");
        }
    }
}
