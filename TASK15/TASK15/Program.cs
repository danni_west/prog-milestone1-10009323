﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter 5 numbers to add them together.");

            Console.WriteLine("Number 1.");
            var first = double.Parse(Console.ReadLine());

            Console.WriteLine("Number 2.");
            var second = double.Parse(Console.ReadLine());

            Console.WriteLine("Number 3.");
            var third = double.Parse(Console.ReadLine());

            Console.WriteLine("Number 4.");
            var fourth = double.Parse(Console.ReadLine());

            Console.WriteLine("Number 5.");
            var fifth = double.Parse(Console.ReadLine());

            var numbers = new List<double> { };
            numbers.Add(first);
            numbers.Add(second);
            numbers.Add(third);
            numbers.Add(fourth);
            numbers.Add(fifth);

            Console.Clear();
            Console.WriteLine(string.Join(",", numbers));
            Console.WriteLine("The total of all numbers entered is..");
            Console.Write(numbers.Sum(x => Convert.ToInt32(x)));
            Console.WriteLine();
        }
    }
}
