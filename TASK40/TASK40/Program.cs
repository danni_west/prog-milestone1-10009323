﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK40
{
    class Program
    {
        static void Main(string[] args)
        {
            var months = new Dictionary<string, double>();

            months.Add("January", 5);
            months.Add("February", 4);
            months.Add("March", 5);
            months.Add("April", 4);
            months.Add("May", 5);
            months.Add("June", 4);
            months.Add("July", 5);
            months.Add("August", 5);
            months.Add("September", 4);
            months.Add("October", 5);
            months.Add("November", 4);
            months.Add("December", 5);

            Console.WriteLine("Assuming that the 1st of the month is on a Monday this is how many Wednesdays are in each month:");

            foreach (var x in months)
            {
                Console.WriteLine($"{x.Key}, {x.Value}");
            }
        }
    }
}
