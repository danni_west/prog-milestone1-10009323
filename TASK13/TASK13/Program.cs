﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK13
{
    class Program
    {
        static void Main(string[] args)
        {
            var GST = 1.15;

            Console.WriteLine("Please enter the price of 3 items you intend on purchasing to work out the total price including GST.");

            Console.WriteLine("Item 1");
            var Item1 = double.Parse(Console.ReadLine());

            Console.WriteLine("Item 2");
            var Item2 = double.Parse(Console.ReadLine());

            Console.WriteLine("Item 3");
            var Item3 = double.Parse(Console.ReadLine());

            var answer = (Item1 + Item2 + Item3) * GST;

            Console.WriteLine($"{Item1} + {Item2} + {Item3} = ${answer} (inc GST)");
        }
    }
}
