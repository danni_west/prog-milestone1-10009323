﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK37
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the amount of 15 credit papers you are currently taking.");
            var a = double.Parse(Console.ReadLine());

            var b = a * 15;
            var c = b * 10;
            var d = a * 5 * 12;
            var e = c - d;
            var f = e / 12;

            Console.WriteLine($"You need to study {f} hours per week outside of tutorial and lecture times.");
        }
    }
}
