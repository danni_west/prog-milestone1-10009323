﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK18
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string, int, string>>();

            names.Add(Tuple.Create("Bob", 30, "July"));
            names.Add(Tuple.Create("Sally", 12, "January"));
            names.Add(Tuple.Create("John", 6, "November"));

            var output = string.Join(", ", names);
            Console.WriteLine(output);
        }
    }
}
