﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type in \"c\" for celsius or \"f\" for fahrenheit");

            var metric = Console.ReadLine();

            switch (metric)
            {
                case "c":
                    Console.WriteLine("Type in a number to convert celsius to fahrenheit");
                    var getcelsius = double.Parse(Console.ReadLine());
                    var roundingcelsius = System.Math.Round((((9.0 / 5.0) * getcelsius) + 32), 2);
                    Console.WriteLine($"{getcelsius} degrees celsius is {roundingcelsius} degrees fahrenheit");
                    break;

                case "f":
                    Console.WriteLine("Type in a number to convert fahrenheit to celsius");
                    var getfahrenheit = double.Parse(Console.ReadLine());
                    var roundingfahrenheit = System.Math.Round((5.0 / 9.0) * (getfahrenheit - 32), 2);
                    Console.WriteLine($"{getfahrenheit} degrees fahrenheit is {roundingfahrenheit} degrees celsius");
                    break;

                default:
                    Console.WriteLine("You had to type in \"c\" for celsius or \"f\" for fahrenheit - Please run the program again");
                    break;
            }
        }
    }
}
