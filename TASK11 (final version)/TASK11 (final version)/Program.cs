﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK11__final_version_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a year in 4 digits");
            int year = Convert.ToInt32(Console.ReadLine());

            if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
            {
                Console.WriteLine($"{year} is a leap year.");
            }

            else
            {
                Console.WriteLine($"{year} is not a leap year.");
            }
        }
    }
}
