﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK05
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the time in military format (e.g 0100)");
            var a = int.Parse(Console.ReadLine());

            var b = a - 1200;
            var c = b / 100;
            var d = a / 100;

            if (a > 1259)
            {
                Console.WriteLine($"The time is {c}pm.");
            }
            else
            {
                Console.WriteLine($"The time is {d}am.");
            }
        }
    }
}
