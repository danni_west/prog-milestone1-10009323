﻿using System;

namespace TASK26
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var colors = new string[5] { "Red", "Blue", "Yellow", "Green", "Pink" };

			Array.Reverse(colors);

			Console.WriteLine(string.Join(",",colors));
		}
	}
}
