﻿using System;

namespace TASK01
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Hello, please enter your name");
			var name = Console.ReadLine();

			Console.WriteLine("Please enter your age");
			var age = Console.ReadLine();

			Console.WriteLine($"Your name is {name} and your age is {age}. Have a nice day {name}!");

			// Console.WriteLine("Your name is " + name + " and your age is " + age + ". Have a nice day " + name + "!.");
			// Console.WriteLine("Your name is {0} and your age is {1}. Have a nice day {2}!", name, age, name);
		}
	}
}
