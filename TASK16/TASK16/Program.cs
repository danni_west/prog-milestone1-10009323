﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word to count the letters in it.");

            string word = Console.ReadLine();
            int length = word.Length;

            Console.WriteLine($"There are {word.Length} letters in the word '{word}'");
        }
    }
}
