﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK17
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string, int>>();

            names.Add(Tuple.Create("Bob", 32));
            names.Add(Tuple.Create("Sally", 25));
            names.Add(Tuple.Create("John", 52));

            var output = string.Join(", ", names);
            Console.WriteLine(output);
        }
    }
}
