﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type in \"k\" for kilometre or \"m\" for miles");

            var metric = Console.ReadLine();
            const double mile2km = 1.609344;
            const double km2mile = 0.621371;

            switch (metric)
            {
                case "k":
                    Console.WriteLine("Type in a number to convert KM to Miles");
                    var getKM = double.Parse(Console.ReadLine());
                    var roundingKM = System.Math.Round(getKM * km2mile, 2);
                    Console.WriteLine($"{getKM} KM is {roundingKM} Miles");
                    break;
                case "m":
                    Console.WriteLine("Type in a number to convert Miles to KM");
                    var getMile = double.Parse(Console.ReadLine());
                    var roundingMile = System.Math.Round(getMile * mile2km, 2);
                    Console.WriteLine($"{getMile} Miles is {roundingMile} KM");
                    break;
                default:
                    Console.WriteLine("You had to type in \"k\" for kilometre or \"m\" for miles - Please run the program again");
                    break;
            }
        }
    }
}
