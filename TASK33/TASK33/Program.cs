﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK33
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the number of students you have.");
            var students = double.Parse(Console.ReadLine());
            var answer = System.Math.Round(students / 28, 2);

            Console.Clear();

            Console.WriteLine($"You will need to have {answer} tutorial groups for {students} students");
        }
    }
}
