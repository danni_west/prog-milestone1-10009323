﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK12
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;

            Console.WriteLine("Please enter a number to check if it is odd or even");
            var userinput = Console.ReadLine();
            num = int.Parse(userinput);

            if (num % 2 == 0)
            {
                Console.WriteLine($"{userinput} is an even number");
            }
            else
            {
                Console.WriteLine($"{userinput} is an odd number");
            }
        }
    }
}
